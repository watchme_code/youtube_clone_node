# YouTube Clone Project
## Features

Our YouTube Clone currently supports a variety of features, making it a comprehensive platform for video sharing and user interaction. The implemented features include:

- **Join**: New users can sign up and create their own accounts to join the community.
- **Login**: Returning users can log in to access their profiles and interact with the platform.
- **Search**: Users can search for videos using keywords, titles, or descriptions.
- **Edit Profile**: Users can edit their profile information to keep their accounts up to date.
- **Change Password**: For security, users can change their passwords.
- **Upload Videos**: Users can upload their own videos to share with others.
- **Edit Video**: Uploaders have the ability to edit their video details (e.g., title, description) after posting.
- **Likes/Dislikes**: Users can express their opinions on videos through likes and dislikes.

### Upcoming Features

We're constantly working to improve the platform and plan to introduce the following features soon:

- **User Detail**: A detailed user profile page, showcasing user information and their video contributions.
- **Advanced Video Detail**: Enhanced video details page, providing more information and interaction options for each video.
